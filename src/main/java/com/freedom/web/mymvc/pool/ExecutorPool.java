package com.freedom.web.mymvc.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.freedom.web.mymvc.utils.PropertiesUtils;

public class ExecutorPool {
	private static final Logger logger = LogManager.getLogger(ExecutorPool.class);

	private ExecutorPool() {
	}

	// 单例模式
	private static ExecutorService instance = null;
	static {
		int consumer_worker_number = PropertiesUtils.getInstance().getConsumerWorker();
		instance = new ThreadPoolExecutor(consumer_worker_number, consumer_worker_number, //
				0L, TimeUnit.MILLISECONDS, //
				new LinkedBlockingQueue<Runnable>(), //
				new DefaultThreadFactory("ExecutorPool-counter"));
	}

	public static ExecutorService getInstance() {
		return instance;
	}
}
