package com.freedom.web.mymvc.route;

import java.lang.reflect.Method;

public class Route {
	private String path;
	private Object object;
	private Method method;

	public String getPath() {
		return path;
	}

	public Route(String path, Object obj, Method met) {
		this.path = path;
		this.object = obj;
		this.method = met;
	}

	public Object getObject() {
		return object;
	}

	public Method getMethod() {
		return method;
	}

}
